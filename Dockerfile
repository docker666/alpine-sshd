FROM alpine:3.15

RUN apk --update --no-cache openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
CMD ["--help"]
